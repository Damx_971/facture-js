window.addEventListener('load', () => {

let companies = [
    {
        id:1, 
        name : "Google"
    },
    {
        id:2, 
        name : "Apple"
    },
    {
        id:3, 
        name : "Facebook"
    },
    {
        id:4, 
        name : "Amazon"
    }
]

//Date de la facture
let date = document.querySelector("#date");
var maintenant=new Date();
var jour=maintenant.getDate();
var mois=maintenant.getMonth()+1;
var an=maintenant.getFullYear();
let datefacture =`${jour}/${mois}/${an}`;
date.innerHTML = datefacture;

//List de clients
let societe = document.querySelector("#societe");

for (let i = 0; i < companies.length; i++) {
    var option = document.createElement('option');
    option.innerHTML= companies[i].name;
    societe.appendChild(option);  
    option.classList.add('societe_select');
    option.setAttribute('value',companies[i].name);
}

//Les types
let typeArray = [
    {
        id:1, 
        name : "Service"
    },
    {
        id:2, 
        name : "Jours"
    },
    {
        id:3, 
        name : "Heures"
    }
]
let types = document.querySelector(".types");

for (let i = 0; i < typeArray.length; i++) {
    var option = document.createElement('option');
    option.innerHTML= typeArray[i].name;
    types.appendChild(option);  
    option.classList.add('types_select');
    option.setAttribute('value',typeArray[i].name);
}

document.addEventListener('change', (event) => {
    //Prix
    let prixunit = document.querySelectorAll(".prixunit");
    let quantite = document.querySelectorAll(".quantite");

    let totalht = document.querySelectorAll(".totalht");
    let totalttc = document.querySelectorAll(".totalttc");
    
    let value_ht = 0;
    let value_ttc = 0;

    for (let  i = 0; i < quantite.length; i++) {  
        //Prix ht
        let totalht_value = prixunit[i].value * quantite[i].value;
        totalht[i].innerHTML = `${totalht_value.toFixed(2)} €`;
        //Prix ttc
        let totalttc_value = prixunit[i].value * quantite[i].value * 1.20;
        totalttc[i].innerHTML = `${totalttc_value.toFixed(2)} €`;

        value_ht = value_ht + totalht_value;
        value_ttc = value_ttc + totalttc_value;
    };

    //Sous total HT
    let sous_total_ht = document.querySelector("#sous_total_ht");
    sous_total_ht.innerHTML = `${value_ht.toFixed(2)} €`;

    //Sous total TTC
    let sous_total_ttc = document.querySelector("#sous_total_ttc");
    sous_total_ttc.innerHTML = `${value_ttc.toFixed(2)} €`;

    //Total
    let total = document.querySelector("#total");
    total.innerHTML = `${value_ttc.toFixed(2)}€`;
});

let facture = document.querySelector("#detailsdelafacture")
let add = document.querySelector("#btn_add");

add.addEventListener('click', (event) =>{
    let ligne = document.querySelectorAll(".facture_ligne");
    let ligne_content = ligne[0].innerHTML;

    //Ajout de ligne
    let ligne_div = document.createElement('div');
    ligne_div.innerHTML= ligne_content;
    facture.appendChild(ligne_div); 
    ligne_div.classList.add('facture_ligne')

    //Supression de ligne
    let remove = document.querySelectorAll(".btn_delete");
    
    for (let i = 0; i < remove.length; i++) {  
        ligne = document.querySelectorAll(".facture_ligne");

        remove[i].addEventListener('click', (event) =>{
            ligne[i].remove();
        });
    };
});


//Créer la facture
let create = document.querySelector("#create");

create.addEventListener('click', (event) => {
    let total_ht = document.querySelector("#sous_total_ht");
    let total_ttc = document.querySelector("#sous_total_ttc");
    let client = document.querySelector("#societe");

    let types = document.querySelectorAll(".types");
    let descriptions = document.querySelectorAll(".description");
    let quantite = document.querySelectorAll(".quantite");
    let prixunit = document.querySelectorAll(".prixunit");
    let ht = document.querySelectorAll(".totalht");
    let ttc = document.querySelectorAll(".totalttc");

    let ligne;
    for (let i = 1; i < types.length; i++) {  
        if(ligne === undefined){
            ligne = [ {Type: types[i].value, Description: descriptions[i].value, quantite: quantite[i].value, Prix_HT: `${prixunit[i].value} €`, Total_HT: ht[i].innerHTML, Total_TTC: ttc[i].innerHTML} ];
        }
        else{
            ligne = [ ligne, {Type: types[i].value, Description: descriptions[i].value, quantite: quantite[i].value, Prix_HT: `${prixunit[i].value} €`, Total_HT: ht[i].innerHTML, Total_TTC: ttc[i].innerHTML} ];
        }
    };

    var obj = {
        lignes: ligne,
        Client: client.value,
        Totat_HT: total_ht.innerHTML,
        Total_TTC: total_ttc.innerHTML
    };

    console.log(obj);
})


});
